#!/bin/bash

##
## Abelohost B.V. random port generator for Vestacp.com
##

#
# Start installation procedure
#

if [ -n "`echo \`uname -a\` | grep -e \"-686\|i686\"`" ]
then
    ARCH=32
fi
if [ -n "`echo \`uname -a\` | grep -e \"amd64\|x86_64\"`" ]
then
    ARCH=64
fi

if [ -n "`cat /etc/issue | grep -i \"Debian\"`" ]
then
    DISTRIB=debian
fi

if [ -n "`cat /etc/issue | grep -i \"CentOS\"`" ]
then
    DISTRIB=centos
fi

if [ -n "`cat /etc/issue | grep -i \"S\"`" ]
then
    DISTRIB=centos
fi

# detect Ubuntu as Debian
if [ -n "`cat /etc/issue | grep -i \"Ubuntu\"`" ]
then
    DISTRIB=debian
fi

echo "We working on $DISTRIB $ARCH"

case $DISTRIB in
    debian)
    directory="/etc/update-motd.d/100-on-login"
    apt-get install nc -y
    ;;

    centos)
    directory="/etc/profile.d/login-info.sh"
    yum install nc -y
    ;;

    *)
    echo "Can't determine OS. Exiting..."
    exit 1
    ;;
esac

port=$(shuf -i 1000-9999 -n 1)
check_port=$(nc 127.0.0.1 $port < /dev/null; echo $?)
if [[ $check_port -eq 0 ]]; then
	echo "Please re-run script";
	exit 1;
else

cat << 'EOF' > $directory
#! /usr/bin/env bash

# Basic info
HOSTNAME=`uname -n`
ROOT=`df -Ph | grep vda1 | awk '{print $4}' | tr -d '\n'`

# System load
MEMORY1=`free -t -m | grep Total | awk '{print $3" MB";}'`
MEMORY2=`free -t -m | grep "Mem" | awk '{print $2" MB";}'`
LOAD1=`cat /proc/loadavg | awk {'print $1'}`
LOAD5=`cat /proc/loadavg | awk {'print $2'}`
LOAD15=`cat /proc/loadavg | awk {'print $3'}`
pub_ip=$(curl -s vestacp.com/what-is-my-ip/)
port=$(cat /usr/local/vesta/nginx/conf/nginx.conf | grep listen | awk '{ print $2 }' | sed 's/;//')
echo "
===============================================
 - Hostname............: $HOSTNAME
 - Disk Space..........: $ROOT remaining
===============================================
 - CPU usage...........: $LOAD1, $LOAD5, $LOAD15 (1, 5, 15 min)
 - Memory used.........: $MEMORY1 / $MEMORY2
 - Swap in use.........: `free -m | tail -n 1 | awk '{print $3}'` MB
===============================================
(!) VESTA panel URL: https://${pub_ip}:${port}
===============================================
"
EOF
chmod 700 $directory

white_subnet=$(iptables-save | grep 'dport 8083 -j ACCEPT' | awk '{ print $4 }')
if [ "${white_subnet}" = "tcp" ]; then
	white_subnet="0.0.0.0/0";
fi

sed -i "s/8083;/${port};/" /usr/local/vesta/nginx/conf/nginx.conf
/usr/local/vesta/bin/v-add-firewall-rule ACCEPT ${white_subnet} ${port} TCP
/usr/local/vesta/bin/v-delete-firewall-rule 2
echo "Restarting VESTA..."
/etc/init.d/vesta restart
echo "White Subnet to access the panel: ${white_subnet}";
# Get public IP
pub_ip=$(curl -s vestacp.com/what-is-my-ip/)
echo "New VESTA URL: https://${pub_ip}:${port}";

fi
exit 0